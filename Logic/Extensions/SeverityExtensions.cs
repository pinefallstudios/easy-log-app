﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Extensions
{
    public static class SeverityExtensions
    {
        public static Severities ToSeverity(this string value)
        {
            int t = -1;
            if (Int32.TryParse(value, out t))
                return t.ToSeverity();
            else
                throw new Exception("ToSeverity: 'value' is not an integer");
        }

        public static Severities ToSeverity(this int value)
        {
            return (Severities)value;
        }
    }
}
