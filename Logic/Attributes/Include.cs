﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Attributes
{
    public class Include : System.Attribute
    {
        public bool Value { get; set; }
        public int SortOrder { get; set; }

        public Include()
        {
            this.Value = true;
            this.SortOrder = 999;
        }

        public Include(int sortOrder = 999) : this()
        {
            this.Value = true;
            this.SortOrder = sortOrder;
        }

        public Include(bool value = true, int sortOrder = 999) : this(sortOrder)
        {
            this.Value = value;
            this.SortOrder = sortOrder;
        }
    }
}
