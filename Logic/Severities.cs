﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic
{
    public enum Severities
    {
        Trace,
        Debug,
        Info,
        Success,
        Warning,
        Error,
    }
}
