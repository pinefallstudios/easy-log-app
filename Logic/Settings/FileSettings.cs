﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentScheduler;

namespace PinefallStudios.EasyLog.Logic.Settings
{
    public enum SplitOptions
    {
        None,
        Daily
    }

    public class FileSettings
    {
        /* Split Options */
        public SplitOptions Split { get; set; }
        public string Location { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        
        public FileSettings()
        {
            this.Split = SplitOptions.None;
        }
        public FileSettings(string location)
        {
            this.Split = SplitOptions.None;
            this.Location = location;
        }

        public FileSettings(string location, SplitOptions split, int hour, int minute) : this(location)
        {
            this.Split = split;
            this.Hour = hour;
            this.Minute = minute;
        }        
    }
}
