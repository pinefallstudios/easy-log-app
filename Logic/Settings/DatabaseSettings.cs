﻿using PinefallStudios.EasyLog.Logic.Interfaces.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Settings
{
    public class DatabaseSettings : IEasyLogDatabaseSettings
    {
        public string ConnectionString { get; set; }
    }
}
