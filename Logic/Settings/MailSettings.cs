﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Settings
{
    public class MailSettings
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Server { get; set; }
        public string Password { get; set; }
        public bool SSL { get; set; }
    }
}
