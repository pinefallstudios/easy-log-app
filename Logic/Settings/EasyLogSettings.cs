﻿using PinefallStudios.EasyLog.Logic.Interfaces.Settings;
using PinefallStudios.EasyLog.Logic.Modules.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Settings
{
    public class EasyLogSettings
    {
        public string ApplicationName { get; set; }
        public string AssemblyName { get; set; }

        public DatabaseSettings Database { get; set; }
        //public IEasyLogFilerSettings Filer { get; set; }

        public EasyLogSettings Clone()
        {
            var clone = new EasyLogSettings();
            clone.ApplicationName = this.ApplicationName;
            clone.AssemblyName = this.AssemblyName;
            clone.Database = this.Database;
            //clone.Filer = this.Filer;
            return clone;
        }

        public EasyLogSettings Clone(string assemblyName)
        {
            var clone = Clone();
            clone.AssemblyName = assemblyName;
            return clone;
        }
    }
}
