﻿using PinefallStudios.EasyLog.Logic.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic
{
    public class Message
    {
        public int ID { get; set; }        
        public DateTime Date { get; set; }
        public Severities Severity { get; set; }
        public string Application { get; set; }
        public string Assembly { get; set; }
        public string Class { get; set; }
        public string Method { get; set; }
        public string Description { get; set; }
        

        public Message(Severities severity, string application, string assembly, string cls, string method, string description)
        {
            this.Severity = severity;
            this.Application = application;
            this.Assembly = assembly;
            this.Class = cls;
            this.Method = method;
            this.Description = description;
            this.Date = DateTime.Now;
        }

        public Message(int id, Severities severity, DateTime date, string application, string assembly, string cls, string method, string description) : this(severity, application, assembly, cls, method, description)
        {
            this.ID = id;
            this.Date = date;
        }

        public override string ToString()
        {
            return "[" + Date.ToString() + "] [" + Severity.ToString() + "] [" + this.Application + "] [" + this.Assembly + "] [" + this.Class + "." + this.Method + "] " + this.Description;
        }
    }
}
