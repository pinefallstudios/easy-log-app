﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Linq;
using PinefallStudios.EasyLog.Logic.Interfaces;
using PinefallStudios.EasyLog.Logic.Settings;
using PinefallStudios.EasyLog.Logic.Modules;
using PinefallStudios.EasyLog.Logic.Interfaces.Settings;
using PinefallStudios.EasyLog.Logic.Interfaces.Modules;
using FluentScheduler;
using PinefallStudios.EasyLog.Logic.Modules.Database;

namespace PinefallStudios.EasyLog.Logic
{
    /* Issues
     * Insert Date: UTC Date
     * Applications table?
     * Source table? (Like user)
        */
    public class EasyLog : IEasyLog 
    {
        public event EventHandler OnAddMessage;
        public event EventHandler OnAddTraceMessage;
        public event EventHandler OnAddDebugMessage;
        public event EventHandler OnAddInfoMessage;
        public event EventHandler OnAddSuccessMessage;
        public event EventHandler OnAddWarningMessage;
        public event EventHandler OnAddErrorMessage;

        /// <summary>
        /// Name of the Application from where EasyLog is being called
        /// </summary>
        public string Application { get; set; }
        public string Assembly { get; set; }
        public List<Message> Messages { get; private set; }


        /* Modules */
        public Database Database { get; set; }
        public IEasyLogFiler Filer { get; set; }
        public IEasyLogMailer Mailer { get; set; }

        /* Settings */
        public EasyLogSettings Settings { get; set; }

        public EasyLog(EasyLogSettings settings)
        {
            this.Settings = settings;

            this.Messages = new List<Message>();

            this.Application = this.Settings.ApplicationName;
            this.Assembly = this.Settings.AssemblyName;

            //Set some sort of flag!
            this.Database = new Modules.Database.Templates.SQLServer(settings.Database);

            //this.Filer = new Filer();
            //this.Filer.Setup(this.Settings.Filer);

            Reload();
        }

        public void Reload()
        {
            try
            {
                if (this.Database != null)
                {
                    this.Messages = this.Database.Select();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public void Reset()
        {
            this.Messages = new List<Message>();
        }
        
        private void InsertIntoFiler(Message message)
        {
            if(this.Filer != null)
                Filer.Insert(message.ToString());
        }

        private void InsertIntoDatabase(Message message)
        {
            if (this.Database != null)
            {
                var id = this.Database.Insert(message);
                message.ID = id;
            } 
            
        }
        
        public string Raw()
        {
            if (this.Filer != null)
                return this.Filer.ToString();
            else
                return "'Filer' has not been initialized";
        }

        /*** ADD *** */
        public virtual Message Add(Severities severity, string application, string assembly, string cls, string method, string description)
        {
            var message = new Message(severity, application, assembly, cls, method, description);
            Console.WriteLine(message.ToString());

            Messages.Add(message);

            InsertIntoFiler(message);
            InsertIntoDatabase(message);

            OnAddMessage?.Invoke(message, new EventArgs());
            return message;
        }

        public virtual Message Add(Severities severity, string cls, string method, string description)
        {
            return Add(severity, this.Application, this.Assembly, cls, method, description);
        }

        public virtual Message AddTrace(string application, string assembly, string cls, string method, string description)
        {
            var message = Add(Severities.Trace, application, assembly, cls, method, description);
            OnAddTraceMessage?.Invoke(message, new EventArgs());
            return message;
        }

        public virtual Message AddTrace(string cls, string method, string description)
        {
            return AddTrace(this.Application, this.Assembly, cls, method, description);
        }

        public virtual Message AddDebug(string application, string assembly, string cls, string method, string description)
        {
            var message = Add(Severities.Debug, application, assembly, cls, method, description);
            OnAddDebugMessage?.Invoke(message, new EventArgs());
            return message;
        }

        public virtual Message AddDebug(string cls, string method, string description)
        {
            return AddDebug(this.Application, this.Assembly, cls, method, description);
        }

        public virtual Message AddError(string application, string assembly, string cls, string method, string description)
        {
            var message = Add(Severities.Error, application, assembly, cls, method, description);
            OnAddErrorMessage?.Invoke(message, new EventArgs());
            return message;
        }

        public virtual Message AddError(string cls, string method, string description)
        {
            return AddError(this.Application, this.Assembly, cls, method, description);
        }

        public virtual Message AddInfo(string application, string assembly, string cls, string method, string description)
        {
            var message = Add(Severities.Info, application, assembly, cls, method, description);
            OnAddInfoMessage?.Invoke(message, new EventArgs());
            return message;
        }

        public virtual Message AddInfo(string cls, string method, string description)
        {
            return AddInfo(this.Application, this.Assembly, cls, method, description);
        }

        public virtual Message AddWarning(string application, string assembly, string cls, string method, string description)
        {
            var message = Add(Severities.Warning, application, assembly, cls, method, description);
            OnAddWarningMessage?.Invoke(message, new EventArgs());
            return message;
        }

        public virtual Message AddWarning(string cls, string method, string description)
        {
            return AddWarning(this.Application, this.Assembly, cls, method, description);
        }

        public virtual Message AddSuccess(string application, string assembly, string cls, string method, string description)
        {
            var message = Add(Severities.Success, application, assembly, cls, method, description);
            OnAddSuccessMessage?.Invoke(message, new EventArgs());
            return message;
        }

        public virtual Message AddSuccess(string cls, string method, string description)
        {
            return AddSuccess(this.Application, this.Assembly, cls, method, description);
        }
        
    }
}
