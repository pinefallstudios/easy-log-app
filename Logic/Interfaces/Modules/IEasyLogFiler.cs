﻿using PinefallStudios.EasyLog.Logic.Interfaces.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Interfaces.Modules
{
    public interface IEasyLogFiler
    {
        IEasyLogFilerSettings Settings { get; set; }

        void Insert(string value);
        void Setup(IEasyLogFilerSettings setting);
        void Split();
    }
}
