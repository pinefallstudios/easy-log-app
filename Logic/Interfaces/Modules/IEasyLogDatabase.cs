﻿using PinefallStudios.EasyLog.Logic.Interfaces.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Interfaces.Modules
{
    public interface IEasyLogDatabase
    {
        IEasyLogDatabaseSettings Settings { get; set; }
        List<Message> Select();
        void Delete(Message message);
        int Insert(Message message);
        void Update(Message message);
    }
}
