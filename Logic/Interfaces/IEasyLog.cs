﻿using PinefallStudios.EasyLog.Logic;
using PinefallStudios.EasyLog.Logic.Interfaces.Modules;
using PinefallStudios.EasyLog.Logic.Interfaces.Settings;
using PinefallStudios.EasyLog.Logic.Modules;
using PinefallStudios.EasyLog.Logic.Modules.Database;
using PinefallStudios.EasyLog.Logic.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Interfaces
{
    public interface IEasyLog
    {
        string Application { get; set; }
        List<Message> Messages { get; }

        Message Add(Severities severity, string application, string assembly, string cls, string method, string description);
        Message Add(Severities severity, string cls, string method, string description);

        Message AddTrace(string application, string assembly, string cls, string method, string description);
        Message AddTrace(string cls, string method, string description);

        Message AddDebug(string application, string assembly, string cls, string method, string description);
        Message AddDebug(string cls, string method, string description);

        Message AddError(string application, string assembly, string cls, string method, string description);
        Message AddError(string cls, string method, string description);

        Message AddInfo(string application, string assembly, string cls, string method, string description);
        Message AddInfo(string cls, string method, string description);

        Message AddWarning(string application, string assembly, string cls, string method, string description);
        Message AddWarning(string cls, string method, string description);

        Message AddSuccess(string application, string assembly, string cls, string method, string description);
        Message AddSuccess(string cls, string method, string description);

        EasyLogSettings Settings { get; set; }

        Database Database { get; set; }
        IEasyLogFiler Filer { get; set; }
        IEasyLogMailer Mailer { get; set; }

        //void Refresh();
        void Reset();
        void Reload();
        string Raw();
    }
}
