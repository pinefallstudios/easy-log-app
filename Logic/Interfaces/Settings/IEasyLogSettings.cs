﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Interfaces.Settings
{
    public interface IEasyLogSettings
    {
        string ApplicationName { get; set; }
        string AssemblyName { get; set; }
        IEasyLogDatabaseSettings Database { get; set; }
        IEasyLogFilerSettings Filer { get; set; }

        IEasyLogSettings Clone();
    }
}
