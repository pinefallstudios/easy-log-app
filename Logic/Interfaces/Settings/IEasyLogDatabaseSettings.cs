﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Interfaces.Settings
{
    public interface IEasyLogDatabaseSettings
    {
        string ConnectionString { get; set; }
    }
}
