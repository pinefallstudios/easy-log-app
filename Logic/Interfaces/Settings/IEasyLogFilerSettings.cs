﻿using PinefallStudios.EasyLog.Logic.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Interfaces.Settings
{
    public interface IEasyLogFilerSettings
    {
        SplitOptions Split { get; set; }
        string Location { get; set; }
        int Hour { get; set; }
        int Minute { get; set; }
    }
}
