﻿using FluentScheduler;
using PinefallStudios.EasyLog.Logic.Interfaces;
using PinefallStudios.EasyLog.Logic.Interfaces.Modules;
using PinefallStudios.EasyLog.Logic.Interfaces.Settings;
using PinefallStudios.EasyLog.Logic.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Modules
{
    public class Filer : IEasyLogFiler
    {
        public IEasyLogFilerSettings Settings { get; set; }

        public void Setup(IEasyLogFilerSettings settings)
        {
            this.Settings = settings;
            if (this.Settings != null)
            {
                /* Setup Schedule */
                if (this.Settings.Split == SplitOptions.Daily)
                {
                    var registry = new Registry();

                    registry.Schedule(() => {
                        Split();
                    }).ToRunEvery(1).Days().At(this.Settings.Hour, this.Settings.Minute);

                    JobManager.Initialize(registry);
                }

                /* Create File */
                if (!File.Exists(this.Settings.Location))
                {
                    var stream = File.Create(this.Settings.Location);
                    stream.Dispose();
                }
            }
            else
            {
                /* Remove Scheduler */
            }
        }

        public void Insert(string value)
        {
            try
            {
                File.AppendAllText(Settings.Location, value + Environment.NewLine);
            }
            catch (Exception)
            {
            }
        }
        
        public void Split()
        {
            try
            {
                var sourcePath = System.IO.Path.GetDirectoryName(this.Settings.Location);

                var destinationPath = Path.Combine(System.IO.Path.GetDirectoryName(this.Settings.Location), "old");
                Directory.CreateDirectory(destinationPath);
                var destinationFileName = DateTime.Now.ToString("ddMMyyyy-HHmmss") + ".log";

                File.Move(this.Settings.Location, Path.Combine(destinationPath, destinationFileName));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override string ToString()
        {
            if (!String.IsNullOrEmpty(this.Settings.Location))
            {
                if (!File.Exists(this.Settings.Location))
                {
                    var stream = File.Create(this.Settings.Location);
                    stream.Dispose();
                }

                return File.ReadAllText(this.Settings.Location);
            }
            else
            {
                return "'logFileLocation' not found: " + this.Settings.Location;
            }
        }
    }
}
