﻿using PinefallStudios.EasyLog.Logic.Interfaces;
using PinefallStudios.EasyLog.Logic.Interfaces.Modules;
using PinefallStudios.EasyLog.Logic.Interfaces.Settings;
using PinefallStudios.EasyLog.Logic.Settings;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Modules.Database
{
    public abstract class Database
    {
        public DatabaseSettings Settings { get; set; }

        public Database(DatabaseSettings settings)
        {
            this.Settings = settings;
        }
        
        public virtual List<Message> Select()
        {
            throw new NotImplementedException();
        }

        public virtual void Delete(Message message)
        {
            throw new NotImplementedException();
        }

        public virtual int Insert(Message message)
        {
            throw new NotImplementedException();
        }

        public virtual void Update(Message message)
        {
            throw new NotImplementedException();
        }

        public virtual SqlConnection GetConnection()
        {
            SqlConnection connection = new SqlConnection(this.Settings.ConnectionString);
            return connection;
        }
    }
}
