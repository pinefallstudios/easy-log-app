﻿using PinefallStudios.EasyLog.Logic.Interfaces;
using PinefallStudios.EasyLog.Logic.Interfaces.Settings;
using PinefallStudios.EasyLog.Logic.Settings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PinefallStudios.EasyLog.Logic.Modules.Database.Templates
{
    public class SQLServer : Database
    {
        /*
            CREATE SCHEMA EasyLog
            CREATE TABLE EasyLog.Messages
            (
	            [ID] INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	            [Date] DATETIME NOT NULL,
	            [Severity] VARCHAR(10) NOT NULL,
	            [Application] VARCHAR(255) NOT NULL,
                [Assembly] VARCHAR(255) NOT NULL,
	            [Class]	VARCHAR(255) NOT NULL,
	            [Method]	VARCHAR(255) NOT NULL,
	            [Description]	VARCHAR(MAX)
            )
        */

        public SQLServer(DatabaseSettings settings) : base(settings)
        {
        }

        public override List<Message> Select()
        {
            var messages = new List<Message>();

            using (SqlConnection connection = this.GetConnection())
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("SELECT [ID], [Date], [Severity], [Application], [Assembly], [Class], [Method], [Description] FROM EasyLog.Messages", connection))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = reader.GetInt32(0);
                        var date = reader.GetDateTime(1);
                        var severityStr = reader.GetString(2);
                        Severities severityValue = (Severities)Enum.Parse(typeof(Severities), severityStr);
                        var application = reader.GetString(3);
                        var assembly = reader.GetString(4);
                        var cls = reader.GetString(5);
                        var method = reader.GetString(6);
                        var description = reader.GetString(7);

                        var message = new Message(id, severityValue, date, application, assembly, cls, method, description);
                        messages.Add(message);
                    }
                }
            }

            return messages;
        }

        public override void Delete(Message message)
        {
            throw new NotImplementedException();
        }

        public override int Insert(Message message)
        {
            using (SqlConnection connection = GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = @"INSERT INTO EasyLog.Messages ([Date], [Severity], [Application], [Assembly], [Class], [Method], [Description]) OUTPUT INSERTED.ID
                                        VALUES (@date, @severity, @application, @assembly, @class, @method, @description)";

                    cmd.Parameters.AddWithValue("@date", message.Date);
                    cmd.Parameters.AddWithValue("@severity", message.Severity.ToString());
                    cmd.Parameters.AddWithValue("@application", message.Application);
                    cmd.Parameters.AddWithValue("@assembly", message.Assembly);
                    cmd.Parameters.AddWithValue("@class", message.Class);
                    cmd.Parameters.AddWithValue("@method", message.Method);
                    cmd.Parameters.AddWithValue("@description", message.Description);

                    connection.Open();
                    var id = (int)cmd.ExecuteScalar();
                    return id;
                }
            }
        }

        public override void Update(Message message)
        {
            throw new NotImplementedException();
        }
    }
}
