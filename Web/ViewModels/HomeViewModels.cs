﻿using PinefallStudios.EasyLog.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PinefallStudios.EasyLog.Web.ViewModels
{
    public class IndexHomeViewModel
    {
        public ListMessageViewModel List { get; set; }
    }
}