﻿using PinefallStudios.EasyLog.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PinefallStudios.EasyLog.Web.ViewModels
{
    public class ListMessageViewModel
    {
        public int Take { get; set; }
        public IEnumerable<string> Applications { get; set; }
        public string SelectedApplication { get; set; }

        public IEnumerable<string> Severities { get; set; }
        public string SelectedSeverity { get; set; }

        public IEnumerable<Message> Messages { get; set; }
    }
}
