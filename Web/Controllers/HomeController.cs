﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PinefallStudios.EasyLog.Logic.Interfaces;
using PinefallStudios.EasyLog.Web.Models;
using Microsoft.Extensions.Options;
using PinefallStudios.EasyLog.Logic;

namespace PinefallStudios.EasyLog.Web.Controllers
{

    [Route("")]
    public class HomeController : Controller
    {
        public IEasyLog Log { get; private set; }


        public HomeController(IEasyLog log)
        {
            this.Log = log;
        }

        [Route("")]
        public IActionResult Index(int take = 100, string application = "", string severity = "")
        {
            this.Log.Reload();

            var messages = this.Log.Messages.Where(c => c.Severity != Severities.Trace).OrderByDescending(c => c.ID).ToList();
            if (!String.IsNullOrEmpty(application) && application != "All")
                messages = messages.Where(c => c.Application == application).ToList();

            if (!String.IsNullOrEmpty(severity) && severity != "All")
            {
                Severities sev = Enum.Parse<Severities>(severity);
                messages = messages.Where(c => c.Severity == sev).ToList();
            }

            messages = messages.Take(take).ToList();


            var model = new ViewModels.IndexHomeViewModel();
            model.List = new ViewModels.ListMessageViewModel();
            model.List.Take = take;

            var applications = this.Log.Messages.Select(c => c.Application).ToList();
            applications.Insert(0, "All");
            model.List.Applications = applications;
            model.List.SelectedApplication = application;

            var severities = Enum.GetNames(typeof(Severities)).ToList();
            severities.Insert(0, "All");
            model.List.Severities = severities;
            model.List.SelectedSeverity = severity;
            
            model.List.Messages = messages;
            return View(model);
        }
        

    }
}
