﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PinefallStudios.EasyLog.Web.Models
{
    public class Filter
    {
        public int Take { get; set; }
        public List<string> Applications { get; set; }
    }
}
